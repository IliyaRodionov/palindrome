<?php

/**
 * Created by PhpStorm.
 * User: Илия
 * Date: 13.07.2018
 * Time: 4:13
 */
class SearchPalindrome{

    private $count = 0;
    private $setPalindrome = array();
    private $flagStartWord = 0;
    /*
     * Если в предложение не является палиндромом и в предложении не найдено палиндромов, выставляется флаг $testWordFlag
     * для обработки предложения по второму алгоритму, который разбивает предложение на слова и проверяет являются ли они палидромом.
     */
    private $testWordFlag = 0;
    private $arraySearhMaxPalindrome = array();

    public function palindrome($str){

        if(is_string($str)){

            $string = trim($str);

            $invertedString = strrev($string);

            if(strnatcasecmp($string, $invertedString) == 0){

                echo "This a palindrome: ".$str;

                return 1;

            } else {

                $string = strtolower($string);

                $string = $this->deletePunctuationMarksReturnString($str);

                $invertedString = strrev($string);


                if (!strnatcasecmp($string, $invertedString)) {

                    echo $str;

                    return 1;

                } else {


                    $len = strlen($string);

                    for ($i = 0; $i < $len; $i++) {

                        if ($string[$i] == $invertedString[$i]) {

                            $this->count++;

                            if (!$this->flagStartWord) {

                                $startLen = $i;
                                $this->flagStartWord = 1;
                            }

                            if ($i == $len - 1) {

                                $lenWordPalindrome = $this->count;
                                $namePalindrome = substr($string, $startLen, $this->count);
                                $this->setPalindrome[$namePalindrome] = $lenWordPalindrome;

                            }

                        } else {

                            if ($this->flagStartWord) {

                                $lenWordPalindrome = $this->count;
                                $namePalindrome = substr($string, $startLen, $this->count);
                                $this->setPalindrome[$namePalindrome] = $lenWordPalindrome;
                                $this->flagStartWord = 0;
                                $this->count = 0;
                                continue;

                            } else
                                continue;

                        }

                    }

                    //Поиск самого длинного палиндрома
                    $lenPalindromeReality = max($this->setPalindrome);
                    $maxPalindrome = array_keys($this->setPalindrome, max($this->setPalindrome));

                    if ($lenPalindromeReality < 2 && $this->testWordFlag == 0) {

                        $this->testWordFlag = 1;

                        echo "First letter - ".$str[0]." \n"."Max palindrome words:\n";


                    } else {
                            foreach ($maxPalindrome as $result) {
                                echo trim($result) . "\n";
                            }
                    }

                }

            if($this->testWordFlag){


                    $testArrayWord = $this->deletePunctuationMarksReturnArray($str);

                    foreach ($testArrayWord as $keyWord){

                        $keyWord = strtolower($keyWord);

                        $invertedString = strrev($keyWord);

                        if (!strnatcasecmp($keyWord, $invertedString)) {

                            $lenWord1 = strlen($keyWord);

                            $this->arraySearhMaxPalindrome[$keyWord] = $lenWord1;


                        } else {

                            $len = strlen($keyWord);

                            for ($i = 0; $i < $len; $i++) {

                                if ($keyWord[$i] == $invertedString[$i]) {

                                    $this->count++;
                                    if (!$this->flagStartWord) {
                                        $startLen = $i;
                                        $this->flagStartWord = 1;
                                    }
                                    if ($i == $len - 1) {

                                        $lenWordPalindrome = $this->count;
                                        $namePalindrome = substr($keyWord, $startLen, $this->count);
                                        $this->setPalindrome[$namePalindrome] = $lenWordPalindrome;

                                    }

                                } else {

                                    if ($this->flagStartWord) {
                                        $lenWordPalindrome = $this->count;
                                        $namePalindrome = substr($keyWord, $startLen, $this->count);
                                        $this->setPalindrome[$namePalindrome] = $lenWordPalindrome;
                                        $this->flagStartWord = 0;
                                        $this->count = 0;
                                        continue;
                                    } else
                                        continue;

                                }

                            }
                            #var_dump($setPalindrome);
                            $lenPalindromeReality = max($this->setPalindrome);

                            if ($lenPalindromeReality < 2) {

                                continue;

                            } else {

                                    foreach ($this->setPalindrome as $key => $value){

                                        $this->arraySearhMaxPalindrome[$key] = $value;

                                    }

                            }

                        }


                    }

                $len = max($this->arraySearhMaxPalindrome);
                $maxPalindrome = array_keys($this->arraySearhMaxPalindrome, $len);

                foreach ($maxPalindrome as $result) {
                    echo trim($result) . "\n";
                }

            }

            }
        } else {
            return "Is not string!";
        }


    }
    //Метод удаляется пробелы и знаки препинания в предложении
    public function deletePunctuationMarksReturnString($str){

        $string = trim($str, '!? ');

        $arrayStringSpaceDelimiter = explode(" ", $string);

        $string = implode($arrayStringSpaceDelimiter);

        $arrayStringCommaDelimiter = explode(",", $string);

        $string = implode($arrayStringCommaDelimiter);

        return $string;

    }
    //Метод разбивает предложение на слова
    public function deletePunctuationMarksReturnArray($str){

        $string = trim($str, '!? ');

        $arrayStringCommaDelimiter = explode(",", $string);

        $string = implode($arrayStringCommaDelimiter);

        $arrayStringSpaceDelimiter = explode(" ", $string);

        #$string = implode($arrayStringSpaceDelimiter);


        return $arrayStringSpaceDelimiter;

    }

}